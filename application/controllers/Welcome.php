<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function index()
	{
		$this->load->view('header');//cargando cabecera
		$this->load->view('welcome_message');//cargando contenido
		$this->load->view('footer');//cargando PIE

	}
	public function universidad()
	{
		$this->load->view('header');//cargando cabecera
		$this->load->view('universidad');//cargando contenido
		$this->load->view('footer');//cargando PIE

	}
	public function ciudad()
	{
		$this->load->view('header');//cargando cabecera
		$this->load->view('ciudad');//cargando contenido
		$this->load->view('footer');//cargando PIE

	}
}//Cierre de la clase
