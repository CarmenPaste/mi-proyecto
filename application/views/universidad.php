<h1>DIRECCION DE LA UTC</h1>
<img src="<?php echo base_url('assets/img/utc.png') ?>" alt="Logo UTC">
  <style>
    #mapa1 {
      width: 100%;
      height: 500px;
      border: 2px solid black;
    }
  </style>
  <br>
  <div id="mapa1"></div>
  <script type="text/javascript">
    function initMap() {
      // creando una coordenada
      var coordenadaCentral = new google.maps.LatLng(-0.9064968414266507, -78.59013043073463);
      var miMapa = new google.maps.Map(
        document.getElementById('mapa1'),
        {
          center: coordenadaCentral,
          zoom: 9,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
      );
      var marcadorUTC= new google.maps.Marker({position:new google.maps.LatLng(-0.9064968414266507, -78.59013043073463),
      map: miMapa, title: 'UTC Matriz',
      icon: '<?php echo base_url('assets/img/utc.png') ?>'
    });
      var marcadorSalache= new google.maps.Marker({position:new google.maps.LatLng(-0.9992989181371056, -78.61890136915684),
      map: miMapa, title: 'UTC Campus Salache'
  });
    var marcadorPujili= new google.maps.Marker({position:new google.maps.LatLng(-0.958123279658539, -78.69586908872745),
    map: miMapa, title: 'UTC Extension Pujili'
  });
    var marcadorLaMana= new google.maps.Marker({position:new google.maps.LatLng(-0.9330094459030878, -79.23743955175773),
    map: miMapa, title: 'UTC Extension LaMana'
  });

    }
  </script>
